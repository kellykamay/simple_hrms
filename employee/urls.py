from django.urls import path

from employee.views import (
  index_view,
  emp_login_view,
  home_view,
  emp_logout_view,
  PersonalDetailView,
  CompanyDetailView,
  )

app_name = 'employee'
urlpatterns = [
    path('', index_view, name='index'),
    path('dashboard/', home_view, name='home'),
    path('dashboard/login/', emp_login_view, name='login'),
    path('dashboard/logout/', emp_logout_view, name='logout'),
    path('dashboard/<pk>/personal-detail/', PersonalDetailView.as_view(), name='personal-detail'),
    path('dashboard/<pk>/company-detail/', CompanyDetailView.as_view(), name='company-detail'),

]