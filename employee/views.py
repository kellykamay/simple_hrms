from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse_lazy

from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from datetime import datetime

from hr.models import Leaves, Announcement, BankAccount

from django.views.generic import (
    DetailView,
    # ListView,
    # UpdateView,
    # DeleteView,
)

@login_required(login_url=reverse_lazy('employee:login'))
def index_view(request):
  return redirect('employee:home')


@login_required(login_url=reverse_lazy('employee:login'))
def home_view(request):

  now = datetime.date(datetime.now())
  annoucements = Announcement.objects.filter(start_date__lte = now, end_date__gte=now).order_by('-pk')
  leaves = Leaves.objects.get(user=request.user)
  
  context = {
    'theemp':request.user,
    'leaves' : leaves,
    'annoucements':annoucements
  }
  return render(request, 'employee/emp_home.html', context)


@method_decorator([login_required(login_url=reverse_lazy('employee:login'))], name='dispatch')
class PersonalDetailView(DetailView):
  model = User
  template_name = 'employee/emp_personal_detail.html'

  def get_context_data(self, **kwargs):
      context = super(PersonalDetailView, self).get_context_data(**kwargs)
      theuser = User.objects.get(pk=self.kwargs["pk"])
      bank = BankAccount.objects.get(user=theuser)

      context['theemp'] = theuser
      context['bank'] = bank

      return context


@method_decorator([login_required(login_url=reverse_lazy('employee:login'))], name='dispatch')
class CompanyDetailView(DetailView):
  model = User
  template_name = 'employee/emp_company_detail.html'

  def get_context_data(self, **kwargs):
      context = super(CompanyDetailView, self).get_context_data(**kwargs)
      theuser = User.objects.get(pk=self.kwargs["pk"])

      context['theemp'] = theuser

      return context


def emp_login_view(request):
  if request.method == 'POST':
    form = AuthenticationForm(data=request.POST)
    
    if form.is_valid():
      user = form.get_user()

      if user.is_active:
        login(request, user)
        
        if 'next' in request.POST:
          return redirect(request.POST.get('next'))
        else:
          return redirect('employee:home')
  else:
    form = AuthenticationForm()
  return render(request, 'employee/emp_login.html', {'form': form})


def emp_logout_view(request):
  logout(request)
  return redirect("employee:login")