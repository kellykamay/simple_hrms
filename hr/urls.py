from django.urls import path

from .views import (
    hr_dashboard_view,
    hr_login_view,
    hr_logout_view,
    get_designation_json,
    
)

from .hrviews import employeecrud
from .hrviews import departmentcrud
from .hrviews import announcementcrud

app_name = 'hr'
urlpatterns = [
    path('', hr_dashboard_view, name='hr-home'),
    path('login/', hr_login_view, name='hr-login'),
    path('logout/', hr_logout_view, name='hr-logout'),
    path('designation/json/get/', get_designation_json, name='hr-get-designation'),
    
    path('employees/', employeecrud.HrEmployeeListView.as_view(), name='hr-employee-list'),
    path('employee/create/', employeecrud.create_employee_view, name='hr-employee-create'),
    path('employee/<pk>/change/', employeecrud.UpdateEmployeeView.as_view(), name='hr-employee-change'),
    path('employee/<pk>/password/', employeecrud.change_password_view, name="employee-change-password"),
    path('employee/<pk>/delete/', employeecrud.DeleteUserView.as_view(), name="employee-delete"),

    path('department/', departmentcrud.DeparmentListView.as_view(), name='hr-department-list'),
    path('department/add/', departmentcrud.create_department_view, name='hr-department-add'),
    path('department/<pk>/delete/', departmentcrud.DeleteDepartmentView.as_view(), name='hr-department-delete'),
    path('department/<pk>/change/', departmentcrud.update_department_view, name='hr-department-change'),

    path('announcements/', announcementcrud.AnnouncementView.as_view(), name='hr-announcement-list'),
    path('announcements/add/', announcementcrud.create_announcement_view, name='hr-announcement-add'),
    path('announcements/<pk>/delete/', announcementcrud.DeleteAnnouncementView.as_view(), name='hr-announcement-delete'),
    path('announcements/<pk>/change/', announcementcrud.UpdateAnnouncementView.as_view(), name='hr-announcement-change')

]