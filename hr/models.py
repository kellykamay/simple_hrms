from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from PIL import Image


class Department(models.Model):
    name = models.CharField(max_length = 50)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Designation(models.Model):
    department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length = 50)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class UserLevel(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Profile(models.Model):
    MALE = 'Male'
    FEMALE = 'Female'
    GENDER =  (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_admin = models.BooleanField(default=False)
    employee_id = models.CharField(max_length=20, blank=True, null=True, unique=True)
    middle_name = models.CharField(max_length=100, blank=True, null=True)
    nationality = models.CharField(max_length=50, blank=True, null=True)
    gender = models.CharField(max_length=6, choices=GENDER, default=MALE)
    date_of_birth = models.DateField(null=True, blank=True)
    religion = models.CharField(max_length=50, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    passport = models.CharField(max_length=50, blank=True, null=True)
    national_id = models.CharField(max_length=50, blank=True, null=True)
    mobile_number = models.CharField(max_length=50, blank=True, null=True)
    extension = models.IntegerField(blank=True, null=True)
    joining_date = models.DateField(null=True, blank=True)
    last_login = models.DateTimeField(null=True, blank=True)
    exit_date = models.DateField(null=True, blank=True)
    avatar = models.ImageField(upload_to='avatar/', default='default.png', blank=True)
    designation = models.ForeignKey(Designation, on_delete=models.CASCADE, null=True, blank=True )
    salary = models.DecimalField(default=0, max_digits=19, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    user_level = models.ForeignKey(UserLevel, on_delete=models.CASCADE, null=True, blank=True)
    personal_email = models.EmailField(null=True, blank=True)

    class Meta:
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"
    

    def __str__(self):
        return '{} {}, {}'.format(self.user.first_name, self.user.last_name, self.middle_name,)
    
    def get_absolute_url(self):
        return reverse("employee:employee-detail", kwargs={"pk": self.user.pk})

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)
    
    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    # overriding the save method and resize image
    def save(self, *args, **kwargs):
        print("inside image manipulation")
        super().save(*args, **kwargs)

        img = Image.open(self.avatar.path)

        if img.height > 300 and img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.avatar.path)


class Leaves(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vacation = models.IntegerField(default=0)
    sick = models.IntegerField(default=0)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = "Leaves"
        verbose_name_plural = "Leaves"
    
    @receiver(post_save, sender=User)
    def create_user_leaves(sender, instance, created, **kwargs):
        if created:
            Leaves.objects.create(user=instance)


class BankAccount(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    branch = models.CharField(max_length=100, blank=True, null=True)
    account = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.user.username
    
    @receiver(post_save, sender=User)
    def create_user_bank_account(sender, instance, created, **kwargs):
        if created:
            BankAccount.objects.create(user=instance)
    
    # @receiver(post_save, sender=User)
    # def save_user_bank_account(sender, instance, **kwargs):
    #     # instance.bankaccount_set.save()
    #     print(instance.bankaccount_set.name)


class Announcement(models.Model):
    title = models.CharField(max_length=100, blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.title