from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse, Http404
from django.urls import reverse_lazy

from datetime import datetime

from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required

from django.db.models.functions import Extract


from .decorators import admin_required

from .models import (
    Department,
    Profile,
)


@login_required(login_url=reverse_lazy('hr:hr-login'))
@admin_required
def hr_dashboard_view(request):
    # if not request.user.profile.is_admin:
    #     return redirect('employee:index')

    now = datetime.now()
    employees = User.objects.all()
    employeesCount = employees.count()
    departmentCount = Department.objects.all().count()

    thismonthbday = Profile.objects.annotate(
        birth_date_month = Extract('date_of_birth', 'month'),
        birth_date_day = Extract('date_of_birth', 'day')
        ).order_by('birth_date_month', 'birth_date_day').filter(
        date_of_birth__month = now.month, date_of_birth__day__gte=now.day)

    bdaytoday = thismonthbday.filter(date_of_birth__day = now.day)

    context = {
        'employeesCount':employeesCount,
        'departmentCount':departmentCount,
        'homeactive': 'active',
        'thismonthbday': thismonthbday,
        'bdaytoday' : bdaytoday,
        'currentmonth' : now
    }
    return render(request, 'hr/hr_home.html', context)


@login_required(login_url=reverse_lazy('hr:hr-login'))
@admin_required
def get_designation_json(request):
    deptid =  request.GET['deptID']
    
    deptarment = Department.objects.get(pk=deptid)
    designation_list = deptarment.designation_set.all().values('id', 'name')
    dl = list(designation_list)
    # print(designation_list)
    return JsonResponse(dl, safe=False)


def hr_login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()

            if user.profile.is_admin:
                login(request, user)
                if 'next' in request.POST:
                    return redirect(request.POST.get('next'))
                else:
                    return redirect('hr:hr-home')

    else:
        form = AuthenticationForm()
    return render(request, 'hr/hr_login.html', {'form': form})


def hr_logout_view(request):
    if request.method == 'POST':
        logout(request)
        return redirect('hr:hr-login')
    else:
        # redundant fix this soon
        logout(request)
        return redirect('hr:hr-login')