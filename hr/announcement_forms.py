from django import forms

from .models import Announcement


class AnnouncementForm(forms.ModelForm):
    
    start_date = forms.DateField(help_text='Format: YYYY-MM-DD', required=False, widget=forms.TextInput(attrs={'class':'datepicker'}))
    end_date = forms.DateField(help_text='Format: YYYY-MM-DD', required=False, widget=forms.TextInput(attrs={'class':'datepicker'}))

    class Meta:
        model = Announcement
        fields = '__all__'
        widgets={
          'note': forms.Textarea(attrs={'class':'materialize-textarea'}),
        }
        labels = {
            "note": "Message"
        }
