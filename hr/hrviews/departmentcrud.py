from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404
from django.urls import reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.contrib import messages

from django.contrib.auth import login
from django.contrib.auth.decorators import login_required

from django.utils.decorators import method_decorator
from django.db import transaction

from ..decorators import admin_required

from ..models import (
    Department,
    Designation,
)


from ..dept_forms import DepartmentForm, DesignationFormset

from django.views.generic import (
    DetailView,
    ListView,
    UpdateView,
    DeleteView,
)


@method_decorator([login_required(login_url=reverse_lazy('hr:hr-login')), admin_required], name='dispatch')
class DeparmentListView(ListView):
    model = Department
    paginate_by = 10
    queryset = Department.objects.all().order_by('pk')

    def get_context_data(self,**kwargs):
        context = super(DeparmentListView,self).get_context_data(**kwargs)
        context['deptlistactive'] = 'active'
        
        return context


@login_required(login_url=reverse_lazy('hr:hr-login'))
@admin_required
def update_department_view(request, pk):

    try:
        thedept = Department.objects.get(pk=pk)

    except Department.DoesNotExist:
        raise Http404("No Department matches the given query.")

    if request.method == 'GET':
        deptform = DepartmentForm(instance=thedept)
        formset = DesignationFormset(queryset=deptform.instance.designation_set.all())


    if request.method == 'POST':
        deptform = DepartmentForm(request.POST, instance=thedept)
        formset = DesignationFormset(request.POST, queryset=deptform.instance.designation_set.all())


        if deptform.is_valid() and formset.is_valid():
            deptdata = deptform.save(commit=False)

            desigdata = formset.save(commit=False)
            
            for desig in desigdata:
                desig.department = deptdata
                desig.save()

            for obj in formset.deleted_objects:
                obj.delete()

            deptdata.save()
            messages.success(request, 'Department has been updated!')
            return redirect('hr:hr-department-list')

    return render(request, 'hr/department_change_form.html', {
        'deptform':deptform,
        'formset':formset,
        'deptlistactive':'active'
    })


@method_decorator([login_required(login_url=reverse_lazy('hr:hr-login')), admin_required], name='dispatch')
class DeleteDepartmentView(DeleteView):
    model = Department
    template_name = 'hr/department_confirm_delete.html'
    success_url = reverse_lazy('hr:hr-department-list')
    success_message = "Deparment was deleted successfully."

    def get_context_data(self,**kwargs):
        context = super(DeleteDepartmentView,self).get_context_data(**kwargs)
        context['deptlistactive'] = 'active'

        return context

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(DeleteDepartmentView, self).delete(request, *args, **kwargs)


@login_required(login_url=reverse_lazy('hr:hr-login'))
@admin_required
@transaction.atomic
def create_department_view(request):
    # this will also create Designation
    
    if request.method == 'GET':
        deptform = DepartmentForm(request.GET or None)
        formset = DesignationFormset(queryset=Designation.objects.none())
    
    elif request.method == 'POST':
        deptform = DepartmentForm(request.POST)
        formset = DesignationFormset(request.POST)

        if deptform.is_valid() and formset.is_valid():
            # first save this department, as its reference will be used in `Designation`
            dept = deptform.save()
            for form in formset:
                # so that `department` instance can be attached.
                desig = form.save(commit=False)
                desig.department = dept
                desig.save()
            messages.success(request, 'New department has been created!')
            return redirect('hr:hr-department-list')

    return render(request, 'hr/department_create_form.html', {
        'deptlistactive':'active',
        'deptform': deptform,
        'formset': formset,
    })
