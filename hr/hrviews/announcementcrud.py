from django.shortcuts import render, redirect
from django.http import Http404
from django.urls import reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.contrib import messages
from datetime import datetime

from django.contrib.auth.decorators import login_required

from django.utils.decorators import method_decorator
from django.db import transaction

from ..decorators import admin_required

from ..models import Announcement


from ..announcement_forms import AnnouncementForm

from django.views.generic import (
    ListView,
    UpdateView,
    DeleteView,
)

@login_required(login_url=reverse_lazy('hr:hr-login'))
@admin_required
@transaction.atomic
def create_announcement_view(request):

    if request.method == 'GET':
        annoform = AnnouncementForm(request.GET or None)

    elif request.method == 'POST':
        annoform = AnnouncementForm(request.POST)

        if annoform.is_valid():
            annoform.save()
            messages.success(request, 'New announcement has been created!')
            return redirect('hr:hr-announcement-list')

    return render(request, 'hr/announcement_create_form.html',{
        'annoform': annoform,
        'annolistactive':'active'
        })


@method_decorator([login_required(login_url=reverse_lazy('hr:hr-login')), admin_required], name='dispatch')
class AnnouncementView(ListView):
    model = Announcement

    def get_context_data(self,**kwargs):
        context = super(AnnouncementView,self).get_context_data(**kwargs)
        annoucements = Announcement.objects.all().order_by('-pk')
        page = self.request.GET.get('page', 1)
        tempdict = {}
        annolist = []
        now = datetime.date(datetime.now())

        for a in annoucements:
            tempdict['pk'] = a.pk
            tempdict['title'] = a.title
            tempdict['start_date'] = a.start_date
            tempdict['end_date'] = a.end_date

            if a.start_date <= now and a.end_date >= now:
                tempdict['status'] = 'active'
            else:
                tempdict['status'] = 'not active'

            annolist.append(tempdict.copy())


        paginator = Paginator(annolist, 10)
        
        try:
            paginated_annolist = paginator.page(page)
        except PageNotAnInteger:
            paginated_annolist = paginator.page(1)
        except EmptyPage:
            paginated_annolist = paginator.page(paginator.num_pages)

        context['annolist']= paginated_annolist
        context['annolistactive'] = 'active'

        return context


@method_decorator([login_required(login_url=reverse_lazy('hr:hr-login')), admin_required], name='dispatch')
class DeleteAnnouncementView(DeleteView):
    model = Announcement
    template_name = 'hr/announcement_confirm_delete.html'
    success_url = reverse_lazy('hr:hr-announcement-list')
    success_message = "Announcement was deleted successfully."

    def get_context_data(self,**kwargs):
        context = super(DeleteAnnouncementView,self).get_context_data(**kwargs)
        context['annolistactive'] = 'active'

        return context

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(DeleteAnnouncementView, self).delete(request, *args, **kwargs)


@login_required(login_url=reverse_lazy('hr:hr-login'))
@admin_required
def update_announcement_view(request, pk):
    try:
        theannouncement = Announcement.objects.get(pk=pk)

    except Announcement.DoesNotExist:
        raise Http404("No Announcement matches the given query.")

    if request.method == 'GET':
        pass


@method_decorator([login_required(login_url=reverse_lazy('hr:hr-login')), admin_required], name='dispatch')
class UpdateAnnouncementView(UpdateView):
    model = Announcement
    form_class = AnnouncementForm
    template_name = 'hr/announcement_change_form.html'

    def get_success_url(self, **kwargs):
        return reverse_lazy('hr:hr-announcement-change', kwargs={'pk': self.kwargs["pk"]})

    def get_context_data(self, **kwargs):
        context = super(UpdateAnnouncementView, self).get_context_data(**kwargs)
        context['annolistactive'] = 'active'

        try:
            theanno = Announcement.objects.get(pk=self.kwargs["pk"])
        except Announcement.DoesNotExist:
            raise Http404("No announcement matches the given query.")

        context['annoform'] = self.form_class(instance=theanno)

        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()

        try:
            theanno = Announcement.objects.get(pk=self.kwargs["pk"])
        except Announcement.DoesNotExist:
            raise Http404("No announcement matches the given query.")

        anno_form = self.form_class(request.POST, instance=theanno)

        if anno_form.is_valid():
            anno_form.save()

            messages.success(request, 'Successfully Updated!')
            return redirect(self.get_success_url())

        else:
            return self.render_to_response(self.get_context_data(
                    annoform=anno_form
                ))