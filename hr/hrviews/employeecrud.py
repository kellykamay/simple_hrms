from django.shortcuts import render, redirect, get_object_or_404
from django.http import Http404
from django.urls import reverse_lazy
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.contrib import messages

from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required

from django.utils.decorators import method_decorator
from django.db import transaction


from ..decorators import admin_required


from ..models import (
    Department,
    Profile,
    BankAccount,
    Leaves,
)


from ..forms import(
    UserForm,
    ProfileForm,
    ChangeUserForm,
    ChangeUserPassword,
    BankForm,
    LeaveForm,

)

from ..dept_forms import DepartmentForm, DesignationFormset
from ..announcement_forms import AnnouncementForm

from django.views.generic import (
    DetailView,
    ListView,
    UpdateView,
    DeleteView,
)

@method_decorator([login_required(login_url=reverse_lazy('hr:hr-login')), admin_required], name='dispatch')
class HrEmployeeListView(ListView):
    queryset = Profile.objects.all()
    paginate_by = 10

    # def get(self, *args, **kwargs):
    #     if not self.request.user.profile.is_admin:
    #         return redirect('employee:index')

    #     return super().get(*args, **kwargs)

    def get_context_data(self,**kwargs):
        context = super(HrEmployeeListView,self).get_context_data(**kwargs)
        
        context['emplistactive'] = 'active'
        

        return context


@login_required(login_url=reverse_lazy('hr:hr-login'))
@admin_required
@transaction.atomic
def create_employee_view(request):
    # print("Called")
    if request.method == 'POST':
        # print("Submitted")
        user_form = UserForm(request.POST)
        profile_form = ProfileForm(request.POST, request.FILES)
        bank_form = BankForm(request.POST)
        leaves_form = LeaveForm(request.POST)

        if user_form.is_valid() and profile_form.is_valid() and bank_form.is_valid() and leaves_form.is_valid():
            user = user_form.save()
            user.refresh_from_db() # This will load the Profile created by Signal

            # Reload the profile from with the profile instance
            profile_form = ProfileForm(request.POST, request.FILES, instance=user.profile) 
            profile_form.full_clean() # Manually clean the form this time. It is implicitly by "is_valid()" method
            profile_form.save()
            
            bank_form = BankForm(request.POST, instance=user.bankaccount_set.get())
            bank_form.full_clean()
            bank_form.save()

            leaves_form = LeaveForm(request.POST, instance=user.leaves_set.get())
            leaves_form.full_clean()
            leaves_form.save()
            
            # thebank = bank_form.save(commit=False)
            # thebank.user = user
            # thebank.save()
            messages.success(request, 'New employee has been created!')
            return redirect('hr:hr-employee-list')
    else:
        user_form = UserForm()
        profile_form = ProfileForm()
        bank_form = BankForm()
        leaves_form = LeaveForm()
        
    return render(request, 'hr/employee_create_form.html', {
        'user_form': user_form,
        'profile_form': profile_form,
        'bank_form': bank_form,
        'leaves_form': leaves_form,
        'emplistactive':'active'
    })


@method_decorator([login_required(login_url=reverse_lazy('hr:hr-login')), admin_required], name='dispatch')
class UpdateEmployeeView(UpdateView):
    model = User
    form_class = ChangeUserForm
    second_form_class = ProfileForm
    third_form_class = BankForm
    fourth_form_class = LeaveForm

    # bank_form = BankForm
    template_name = 'hr/user_change_form.html'
    # success_url = reverse_lazy('hr:hr-employee-list')
    
    def get_success_url(self, **kwargs):
        return reverse_lazy('hr:hr-employee-change', kwargs={'pk': self.kwargs["pk"]})

    def get_context_data(self, **kwargs):
        context = super(UpdateEmployeeView, self).get_context_data(**kwargs)
        theuser = User.objects.get(pk=self.kwargs["pk"])
        context['emplistactive'] = 'active'
        
        try:
            thebank = BankAccount.objects.get(user=theuser)
        except BankAccount.DoesNotExist:
            raise Http404("No Leaves matches the given query.")
            # TODO: Create custom 404 page
        
        try:
            theleaves = Leaves.objects.get(user=theuser)
        except Leaves.DoesNotExist:
            raise Http404("No Leaves matches the given query.")
            # TODO: Create custom 404 page

        context['userid'] = self.kwargs["pk"]
        context['profilepic'] = theuser.profile.avatar.url
        # context['currentuser'] = self.request.user
        
        try:
            thedesig = theuser.profile.designation
        except ObjectDoesNotExist:
            pass
        try:
            thedept = Department.objects.filter(designation__name=thedesig.name)
            context['deptid'] = thedept[0].id
        except:
            context['deptid'] = 0

        if 'user_form' not in context:
            context['user_form'] = self.form_class(instance=theuser)
        
        if 'profile_form' not in context:
            context['profile_form'] = self.second_form_class(instance=theuser.profile)
        
        if 'bank_form' not in context:
            context['bank_form'] = self.third_form_class(instance=thebank)
        
        if 'leave_form' not in context:
            context['leave_form'] = self.fourth_form_class(instance=theleaves)
        
        return context
    
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        theuser = User.objects.get(pk=self.kwargs["pk"])
        user_form = self.form_class(request.POST, instance=theuser)
        profile_form = self.second_form_class(request.POST, request.FILES, instance=theuser.profile)
        
        try:
            thebank = BankAccount.objects.get(user=theuser)
        except BankAccount.DoesNotExist:
            raise Http404("No Leaves matches the given query.")
            # TODO: Create custom 404 page
        
        try:
            theleaves = Leaves.objects.get(user=theuser)
        except Leaves.DoesNotExist:
            raise Http404("No Leaves matches the given query.")
            # TODO: Create custom 404 page

        bank_form = self.third_form_class(request.POST, instance=thebank)
        leave_form = self.fourth_form_class(request.POST, instance=theleaves)

        if user_form.is_valid() and profile_form.is_valid() and bank_form.is_valid() and leave_form.is_valid():
            userdata = user_form.save(commit=False)
            employeedata = profile_form.save(commit=False)
            employeebank = bank_form.save(commit=False)
            employeeleaves = leave_form.save(commit=False)

            employeedata.user = userdata
            employeebank.user = userdata
            employeeleaves.user = userdata
            
            userdata.save()
            employeedata.save()
            employeebank.save()
            employeeleaves.save()

            messages.success(request, 'Successfully Updated!')
            return redirect(self.get_success_url())
            
        else:
            return self.render_to_response(self.get_context_data(
                user_form=user_form,
                profile_form=profile_form,
                bank_form=bank_form,
                leave_form=leave_form
                # userid=userid,
                # profilepic=profilepic,
                # deptid=deptid
                ))


@login_required(login_url=reverse_lazy('hr:hr-login'))
@admin_required
def change_password_view(request, pk):
    user = User.objects.get(pk=pk)
    if request.method == 'POST':
        password_form = ChangeUserPassword(request.POST, user=user)
        if password_form.is_valid():
            password_form.save()
            messages.success(request, 'Password has been updated!')
            return redirect('hr:hr-employee-change', pk=pk)
    else:
        password_form = ChangeUserPassword(user=user)
    
    return render(request, 'hr/change_password_form.html', {
        'password_form': password_form,
        'userid':pk
    })


@method_decorator([login_required(login_url=reverse_lazy('hr:hr-login')), admin_required], name='dispatch')
class DeleteUserView(DeleteView):
    model = User
    template_name = 'hr/user_confirm_delete.html'
    success_url = reverse_lazy('hr:hr-employee-list')
    success_message = "User was deleted successfully."

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(DeleteUserView, self).delete(request, *args, **kwargs)
