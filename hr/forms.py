from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, ReadOnlyPasswordHashField
from django.utils.safestring import mark_safe

# from string import Template
# from django.forms import ImageField

from .models import Profile, Department, UserLevel, BankAccount, Designation, Leaves


class UserForm(UserCreationForm):
    email = forms.EmailField(
        required=False,
        widget=forms.TextInput(attrs={'type':'email','class':'validate'}),
        help_text=mark_safe('<span class="helper-text" data-error="Wrong email format" data-success="Correct email format">Please enter valid email</span>')
    )
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'password1', 'password2')


class ChangeUserForm(UserChangeForm):
    
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'password')


class ChangeUserPassword(forms.Form):
    password1 = forms.CharField(label='New password', widget=forms.PasswordInput())
    password2 = forms.CharField(label='New password confirmation', widget=forms.PasswordInput())
    
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(ChangeUserPassword, self).__init__(*args, **kwargs)
    
    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password_length = 8
        if len(password1) < password_length:
            raise forms.ValidationError("Password must be longer than {} characters".format(password_length))
        password2 = self.cleaned_data.get('password2')
        if password1 != password2:
            raise forms.ValidationError("The two password fields didn't match.")
    
    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['password2'])
        if commit:
            self.user.save()
        return self.user


# class PictureWidget(forms.widgets.Widget):
#     def render(self, name, value, attrs=None):
#         html =  Template("""<img src='$link'/> """)
#         return mark_safe(html.substitute(link=value))


class ProfileForm(forms.ModelForm):
    EMPTY_CHOICES = [('','---------')]
    GENDER_CHOICES = [('Male','Male'), ('Female','Female')]

    date_of_birth = forms.DateField(help_text='Format: YYYY-MM-DD', required=False, widget=forms.TextInput(attrs={'class':'datepicker'}))
    avatar=forms.ImageField(help_text='Dimension must be 300x300 px', required=False, widget = forms.FileInput(attrs = {'onchange' : "readURL(this);"}))
    # avatar=forms.ImageField()
    gender = forms.ChoiceField(choices=GENDER_CHOICES, required=False, widget=forms.Select(attrs={'class':'myselect'}))
    department = forms.ModelChoiceField(queryset=Department.objects.all(), widget=forms.Select(attrs={'class':'myselect'}),required=False)
    designation = forms.ModelChoiceField(queryset=Designation.objects.all(), required=False, widget=forms.Select(attrs={'class':'myselect'}))
    user_level = forms.ModelChoiceField(queryset=UserLevel.objects.all(),  widget=forms.Select(attrs={'class':'myselect'}),required=False)
    is_admin = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={'class':'filled-in'}))
    extension = forms.IntegerField(label="Extension Number", required=False, widget=forms.TextInput(attrs={'type':'number'}))
    personal_email = forms.EmailField(
        required=False,
        widget=forms.TextInput(attrs={'type':'email','class':'validate'}),
        help_text=mark_safe('<span class="helper-text" data-error="Wrong email format" data-success="Correct email format">Please enter valid email</span>')
    )
    joining_date = forms.DateField(help_text='Format: YYYY-MM-DD', required=False, widget=forms.TextInput(attrs={'class':'datepicker'}))
    vacation = forms.IntegerField(help_text='Number of days', label="Vacation leave", required=False, widget=forms.TextInput(attrs={'type':'number'}))
    sick = forms.IntegerField(help_text='Number of days', label="Sick leave", required=False, widget=forms.TextInput(attrs={'type':'number'}))

    class Meta:
        model = Profile
        exclude = ['user']


class BankForm(forms.ModelForm):
    class Meta:
        model = BankAccount
        fields = ['name', 'branch', 'account']


class LeaveForm(forms.ModelForm):
    class Meta:
        model = Leaves
        exclude = ['user']
