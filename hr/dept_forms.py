from django import forms
from django.forms import modelformset_factory

from .models import Department, Designation


class DepartmentForm(forms.ModelForm):
    class Meta:
        model = Department
        fields = ['name']


class DesignationForm(forms.ModelForm):
    class Meta:
        model = Designation
        fields = ['name']
        labels = {
            "name": "Designation"
        }
        widgets={
        'name': forms.TextInput(
            attrs={
                'placeholder': 'Designation name'
                }
            )
        }


DesignationFormset = forms.modelformset_factory(
    Designation,
    form = DesignationForm,
    can_delete=True,
)
