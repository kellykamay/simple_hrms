from django.contrib import admin

from .models import (
  Profile,
  Department,
  Designation,
  Leaves,
  UserLevel,
  BankAccount,
  Announcement,
  )


admin.site.register(Profile)
admin.site.register(Department)
admin.site.register(Designation)
admin.site.register(Leaves)
admin.site.register(UserLevel)
admin.site.register(BankAccount)
admin.site.register(Announcement)